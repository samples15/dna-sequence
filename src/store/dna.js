// Export
export default {
  namespaced: true,

  // State
  state: {
    dna: localStorage.getItem('dna') ? localStorage.getItem('dna') : null,
  },

  // Getters
  getters: {
    dnaSeq: (state) => state.dna,
  },

  // Actions
  actions: {
    requestSaveDna({ commit }, str) {
      commit('setUserDNA', str);
      localStorage.setItem('dna', str);
    },
  },

  // Mutations
  mutations: {
    setUserDNA(state, payload) {
      state.dna = payload;
    },
  },
};
