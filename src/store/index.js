// Vue
import Vue from 'vue';
import Vuex from 'vuex';

// Modules
import dnaStore from './dna';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    dna: dnaStore,
  },
});

export default store;
